<?php

namespace Webest\Sphinx;

use Foolz\SphinxQL\Exception\ConnectionException;
use Foolz\SphinxQL\Exception\DatabaseException;
use Foolz\SphinxQL\Drivers\Mysqli\Connection;
use Foolz\SphinxQL\SphinxQL;
use Illuminate\Support\Collection;
use Laravel\Scout\Builder;
use Laravel\Scout\Engines\Engine;

class ScoutSphinxEngine extends Engine
{
    protected $connection;

    public function __construct()
    {
        $this->connection = new Connection();
    }

    public function update($models)
    {
        if ($models->isEmpty()) {
            return;
        }

        $index = config('sphinx.index');

        foreach ($models as $model) {
            $attributes = $model->toSearchableArray();

            SphinxQL::update($index)
                ->set($attributes)
                ->where('id', '=', $model->getKey())
                ->execute();
        }
    }

    public function delete($models)
    {
        if ($models->isEmpty()) {
            return;
        }

        $index = config('sphinx.index');

        foreach ($models as $model) {
            SphinxQL::delete()
                ->from($index)
                ->where('id', '=', $model->getKey())
                ->execute();
        }
    }

    public function search(Builder $builder)
    {
        $index = config('sphinx.index');

        $searchQuery = SphinxQL::select()
            ->from($index)
            ->match('title', $builder->query)
            ->execute();

        $results = $searchQuery->fetchAllAssoc();

        return $results;
    }

    public function paginate(Builder $builder, $perPage, $page)
    {
        $index = config('sphinx.index');

        $searchQuery = SphinxQL::select()
            ->from($index)
            ->match('title', $builder->query)
            ->limit($perPage)
            ->offset(($page - 1) * $perPage)
            ->execute();

        return $searchQuery->fetchAllAssoc();
    }

    public function mapIds($results)
    {
        return collect($results)->pluck('id');
    }

    public function map(Builder $builder, $results, $model)
    {
        $ids = $this->mapIds($results);
        return $model->whereIn('id', $ids)->get();
    }

    public function lazyMap(Builder $builder, $results, $model)
    {
        return (new Collection($results))->map(function ($result) use ($model) {
            return $model->newFromBuilder($result);
        });
    }

    public function getTotalCount($results)
    {
        return count($results);
    }

    /**
     * @throws ConnectionException
     * @throws DatabaseException
     */
    public function flush($model)
    {
        $index = config('sphinx.index');
        $this->connection->query("DELETE FROM $index WHERE model_id = {$model->id}");
    }

    /**
     * @throws ConnectionException
     * @throws DatabaseException
     */
    public function createIndex($name, array $options = [])
    {
        $indexDefinition = "CREATE RT INDEX $name";

        if (!empty($options)) {
            $indexDefinition .= ' WITH (' . implode(', ', $options) . ')';
        }

        $this->connection->query($indexDefinition);
    }

    /**
     * @throws ConnectionException
     * @throws DatabaseException
     */
    public function deleteIndex($name)
    {
        $this->connection->query("DROP INDEX $name");
    }
}
