<?php

return [

    'driver' => env('SCOUT_DRIVER', 'sphinx'),

    'sphinx' => [
        'host' => env('SPHINX_HOST', 'localhost'), // Хост Sphinx
        'port' => env('SPHINX_PORT', 9312), // Порт Sphinx
    ],

    /*
    |--------------------------------------------------------------------------
    | Sphinx Index Configuration
    |--------------------------------------------------------------------------
    */

    'index' => 'main_index', // Имя основного индекса Sphinx

    /*
    |--------------------------------------------------------------------------
    | Sphinx Query Configuration
    |--------------------------------------------------------------------------
    */

    'max_results' => 1000, // Максимальное количество результатов поиска
    'match_mode' => 'SPH_MATCH_ALL', // Режим сопоставления (например, SPH_MATCH_ALL, SPH_MATCH_ANY)
    'ranking_mode' => 'SPH_RANK_PROXIMITY_BM25', // Режим ранжирования (например, SPH_RANK_PROXIMITY_BM25)

    /*
    |--------------------------------------------------------------------------
    | Sphinx Connection Configuration
    |--------------------------------------------------------------------------
    */

    'connection_timeout' => 30, // Таймаут соединения с Sphinx
    'query_timeout' => 300, // Таймаут выполнения запроса к Sphinx

];