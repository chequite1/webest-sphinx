<?php

namespace Webest\Sphinx;

use Illuminate\Support\ServiceProvider;

class SphinxServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/config/sphinx.php', 'sphinx');
    }
}